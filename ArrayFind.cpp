#include "ArrayFind.hpp"
#include <iostream>

size_t sumFromZeroToN(size_t n) {
    return n * (n + 1) / 2;
}


std::pair<size_t, size_t> FindMissing(const std::vector<size_t> &array) {
    size_t n = array.size() + 1;

    size_t sum = sumFromZeroToN(n);

    size_t sumArray = 0;
    for (auto i : array) {
        sumArray += i;
    }

    size_t sumMissingPair = sum - sumArray;
    size_t missingPairMedian = sumMissingPair / 2;

    size_t sumToMedian = sumFromZeroToN(missingPairMedian);
    size_t sumToMedianArray = 0;
    for (auto i : array) {
        if (i <= missingPairMedian) {
            sumToMedianArray += i;
        }
    }

    size_t first = sumToMedian - sumToMedianArray;
    size_t second = sumMissingPair - first;

    return std::make_pair(std::min(first, second), std::max(first, second));
}
