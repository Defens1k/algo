#pragma once
#include <utility>
#include <vector>
#include <stddef.h>

std::pair<size_t, size_t> FindMissing(const std::vector<size_t> & array);
