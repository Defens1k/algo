#include <gtest/gtest.h>
#include "ArrayFind.hpp"
#include <iostream>

TEST(findArray, simple) {
    std::vector<size_t> array = {0, 1, 2, 3, 5, 6, 7, 9};
    auto result = FindMissing(array);
    ASSERT_TRUE(result.first == 4);
    ASSERT_TRUE(result.second == 8);
}
TEST(findArray, empty) {
    std::vector<size_t> array = {};
    auto result = FindMissing(array);
    ASSERT_TRUE(result.first == 0);
    ASSERT_TRUE(result.second == 1);
}
TEST(findArray, first) {
    std::vector<size_t> array = {0};
    auto result = FindMissing(array);
    ASSERT_TRUE(result.first == 1);
    ASSERT_TRUE(result.second == 2);
}
TEST(findArray, first2) {
    std::vector<size_t> array = {1};
    auto result = FindMissing(array);
    ASSERT_TRUE(result.first == 0);
    ASSERT_TRUE(result.second == 2);
}
TEST(findArray, first3) {
    std::vector<size_t> array = {2};
    auto result = FindMissing(array);
    ASSERT_TRUE(result.first == 0);
    ASSERT_TRUE(result.second == 1);
}
TEST(findArray, two) {
    std::vector<size_t> array = {2, 1};
    auto result = FindMissing(array);
    ASSERT_TRUE(result.first == 0);
    ASSERT_TRUE(result.second == 3);
}
TEST(findArray, two2) {
    std::vector<size_t> array = {3, 0};
    auto result = FindMissing(array);
    ASSERT_TRUE(result.first == 1);
    ASSERT_TRUE(result.second == 2);
}
TEST(findArray, unsorted) {
    std::vector<size_t> array = {7, 4,6,9,1,3,5,2};
    auto result = FindMissing(array);
    ASSERT_TRUE(result.first == 0);
    ASSERT_TRUE(result.second == 8);
}

