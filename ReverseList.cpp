#include "ReverseList.hpp"
#include <iostream>



template<typename T>
void List<T>::pushFront(const T &value) {
    _frontNode = std::make_shared<Node<T>>(value, _frontNode);
}

template<typename T>
void List<T>::pushBack(const T &value) {
    if (_frontNode == nullptr) {
        _frontNode = std::make_shared<Node<T>>(value);
        return;
    }
    auto ptr = _frontNode;
    auto prev = ptr;
    while (ptr != nullptr) {
        prev = ptr;
        ptr = ptr->nextNode();
    }
    prev->setNext(std::make_shared<Node<T>>(value));
}

template<typename T>
void List<T>::print() {
    auto ptr = _frontNode;
    std::cout << "list: ";
    while (ptr != nullptr) {

        std::cout << ptr->value() << " -> ";
        ptr = ptr->nextNode();
    }
    std::cout << "nullptr" << std::endl;

}

template<typename T>
void List<T>::reverse() {
    if (_frontNode == nullptr) {
        return;
    }
    std::shared_ptr<Node<T>> reversed(nullptr);
    auto nextUnreversed = _frontNode->nextNode();
    while (nextUnreversed != nullptr) {
        _frontNode->setNext(reversed);
        reversed = _frontNode;
        _frontNode = nextUnreversed;
        nextUnreversed = _frontNode->nextNode();
    }
    _frontNode->setNext(reversed);
}


template<typename T>
Node<T>::Node(T value, std::shared_ptr<Node<T>> nextNode) {
    _value = std::move(value);
    _next = std::move(nextNode);
}

template<typename T>
std::shared_ptr<Node<T> > Node<T>::nextNode() {
    return _next;
}

template<typename T>
void Node<T>::setNext(std::shared_ptr<Node<T>> nextNode) {
    _next = nextNode;
}

template<typename T>
const T &Node<T>::value() {
    return _value;
}

template class List<int>;
