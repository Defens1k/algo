#pragma once
#include <memory>



template<typename T>
class Node {
public:
    Node(T value, std::shared_ptr<Node<T>> nextNode = nullptr);
    std::shared_ptr<Node<T>> nextNode();
    void setNext(std::shared_ptr<Node<T>> nextNode);
    const T & value();
private:
    std::shared_ptr<Node<T>>  _next;
    T _value;
};



template <typename T>
class List {
public:
    void pushFront(const T & value);
    void pushBack(const T & value);

    void print();

    void reverse();

private:
    std::shared_ptr<Node<T>> _frontNode;
};


