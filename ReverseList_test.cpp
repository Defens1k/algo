#include <gtest/gtest.h>
#include "ReverseList.hpp"

TEST(reverseList, pushBack) {
    List<int> list;
    for (int i = 0; i < 30; i++) {
        ASSERT_NO_THROW( list.pushBack(i));
        ASSERT_NO_THROW( list.pushFront(i));
    }
    list.print();
}

TEST(reverseList, reverse) {
    List<int> list;
    for (int i = 0; i < 30; i++) {
        list.pushBack(i);
    }
    list.print();
    list.reverse();
    std::cout << "after reverse:" << std::endl;
    list.print();
}

TEST(reverseList, reverse_0) {
    List<int> list;
    list.print();
    list.reverse();
    std::cout << "after reverse:" << std::endl;
    list.print();
}

TEST(reverseList, reverse_1) {
    List<int> list;
    list.pushBack(1);
    list.print();
    list.reverse();
    std::cout << "after reverse:" << std::endl;
    list.print();
}
